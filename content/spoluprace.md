+++
title = "Chci pomoci"
id = "spoluprace"
+++

# Pojďme společně změnit Plasko k lepšímu!
Čekání a nadávání na poměry nic nezmění – Váš čas a Vaše energie ano!

### <i class="far fa-comment"></i> Mluvte o nás
Vidíte to [podobně jako my](/program)? Mluvte o nás a našem programu se svými příteli a známými. To je ta největší pomoc <i class="far fa-heart"></i>

### <i class="far fa-comment"></i> Dejte si náš banner na plot
Připravujeme bannery / plachty k zavěšení na plot, z okna, apod. Jejich rozměry můžou být různé, **např. 1.5m x 0.75m**. A jako bonus Vám ta plachta zbyde po volbách a můžete ji používat třeba na přikrývání pískoviště <i class="far fa-smile-wink"></i> (je z kvalitního PVC a po stranách má ocelová oka).

Ukázka banneru (nápisy i rozměry lze změnit, pokud chcete, tak aby co nejlépe odpovídali Vám i danému místu):

![banner spoluprace](/img/spoluprace/banner_neon.png)

### <i class="far fa-money-bill-alt"></i> Podpořte nás finančně
Jsme občanská kandidátka a narozdíl od politických stran si vše platíme ze svého. Náklady na tisk, nějaká ta reklama, ... - to vše něco stojí, přestože co jde, zvládáme sami.
Přispět nám můžete na náš [transparentní účet: 2901468582 / 2010](https://ib.fio.cz/ib/transparent?a=2901468582). Za každou částku děkujeme.

### <i class="far fa-comments"></i> Kontakt
[Nezávislí pro Plasko na Facebooku](https://www.facebook.com/NezavisliProPlasko/)

Kontaktní formulář:

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdPcR_6vYx0XsCQ9LzyKbC2idF1sqfc9c1HoqqmV7muGb0LMA/viewform?embedded=true" width="700" height="720" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
