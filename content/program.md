+++
title = "Program"
id = "program"
+++
Dnešní stranické vedení města vládne už 8 let. Udělali leccos dobrého, ač při tom město **řádně zadlužili** až do roku 2033. **Město ale nedělají jenom opravené chodníky, ale hlavně lidé, co v něm žijí**. A ti se dneska obcházejí a zapomíná se na ně. A bude to tak i nadále, pokud to nezměníme.
