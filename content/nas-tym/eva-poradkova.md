+++
title = "Mgr. Eva Pořádková"
description = "Eva Pořádková, Kandidátka za Nezávislé pro Plasko"
og_image = "/img/people/poradkova.jpg"
image = "poradkova.jpg"
type = "people"
+++
_"Plasy jako domov pro všechny generace. Pomůžeme setkávání rodičů s dětmi stejně jako seniorů. Pro mladé chceme vybudovat outdoorové sportoviště."_

Pracuje v neziskové organizaci věnující se mimo jiné pomoci školám a obcím a jejich spolupráci v rámci regionu.

Absolvovala gymnázium v Plasích a následně vystudovala právo na Masarykově Univerzitě v Brně.

Po studiu pracovala několik let jako právní čekatelka a asistentka na státních zastupitelstvích v rámci Plzeňského kraje.

Přečtěte si:

* [Informace, otevřená radnice](/aktuality/informace-otevrena-radnice)
* [Upozaděné vzdělávání](/aktuality/upozadene-vzdelani/)
* [Nekoncepční in-line dráha](/aktuality/nekoncepcni-in-line-draha/)
* [Jak nás město neinformuje](/aktuality/jak-nas-mesto-neinformuje/)
