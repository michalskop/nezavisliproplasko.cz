+++
title = "Ing. Václav Gross"
description = "Ing. Václav Gross Kandidát za Nezávislé pro Plasko"
og_image = "/img/people/gross.jpg"
image = "gross.jpg"
type = "people"
+++
_"Plasy, to jsou také okolní vesnice. Dáme jejich osadním výborům větší prostor v rozhodování o vlastních záležitostech."_

Je absolventem Západočeské univerzity v Plzni v oboru Strojírenská technologie.

Pracuje jako vedoucí mechanik ve společnosti LB MINERALS s.r.o.

V posledních 12 letech byl ve vedení města Plasy. V letech 2006 - 2010 jako radní a předseda finančního výboru a v letech 2010 - 2018 jako místostarosta obce.

Přečtěte si:

* [Rozumné hospodaření](/aktuality/rozumne-hospodareni/)
* [Přidružené obce města Plasy - financování](/aktuality/pridruzene-obce-financovani/)
* [Finanční situace Plas z pohledu místostarosty](/aktuality/financni-situace-plas-gross/)
* [Vodovod v Žebnici](/aktuality/vodovod-zebnice/)
