+++
title = "Jan Jakeš"
description = "Jan Jakeš Kandidát za Nezávislé pro Plasko"
og_image = "/img/people/jakes.jpg"
image = "jakes.jpg"
type = "people"
+++
_"Věřím ve výchovu mladé generace ke slušnosti a férovosti pomocí sportu a kroužků. Je pozdě chtít měnit patnáctileté grázlíky."_

Je plaským rodákem. Celá rodina pochází s Plas a také v Plasích žije.

Pracoval téměř 15 let jako profesionální hasič. Nyní je v privátní sféře.

Třetím rokem zastává funkci předsedy TJ Plasy.

Přečtěte si:

* [Sport a volný čas](/aktuality/sport-a-volby-cas/)
* [Sport v Plasích](/aktuality/sport-v-plasich/)
