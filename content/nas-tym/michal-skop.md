+++
title = "Mgr. Michal Škop, Ph.D."
description = "Mgr. Michal Škop, Ph.D. Kandidát za Nezávislé pro Plasko"
og_image = "/img/people/skop.jpg"
image = "skop.jpg"
type = "people"
+++
_"Učíme se od nejlepších. Nejdříve okoukat, kde co dobře funguje, pak to teprve začít dělat. Zapojení lidí do dění ve městě jako v Brně, nulové dluhy jako v Třemošné."_

Pracuje jako expert pro otevřená data Ministerstva vnitra, analytik a programátor.

Je autorem populární [Volební kalkulačky](https://volebnikalkulacka.cz).

Několik let vedl českou a slovenskou pobočku americké firmy StatSoft.

Učil na univerzitách v Praze, Olomouci, Hradci Králové a v Plzni. Studoval na Univerzitě Karlově, v Max-Planck Institutu v Německu a na Univerzitě v Salamance ve Španělsku.

V Plasích žije od 2 let.

Přečtěte si:

* [Město pro všechny](/aktuality/mesto-pro-vsechny/)
* [Plasy - nejzadluženější město ČR 2018](https://skop.eu/post/nejzadluzenejsi-mesto-cr-2018/)
* [Kino - náklady](/aktuality/kino-naklady/)
* [Informace o plánované in-line dráze](/aktuality/in-line-info/)
