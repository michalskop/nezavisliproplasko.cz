+++
title = "Mgr. Martin Fazekaš"
description = "Mgr. Martin Fazekaš Kandidát za Nezávislé pro Plasko"
og_image = "/img/people/fazekas.jpg"
image = "fazekas.jpg"
type = "people"
+++
_"Naše škola i školka už nějakou dobu praskají ve švech. Musíme to vyřešit co nejdříve. Vzdělání je to nejlepší, co můžeme dětem dát."_

Narodil se v Sokolově, ale část kořenů má v Plasích.

Je absolventem pedagogické fakulty ZČU.

Pracuje jako učitel v Kralovicích.

Několik let působil ve Školské radě ZŠ Plasy. Je zastupitelem města Plasy a členem kulturní komise.

Je členem Sokola Plasy.

V současnosti studuje strojní fakultu ZČU.

Přečtěte si:

* [Dobrá škola je základ](/aktuality/dobra-skola-je-zaklad/)
* [Krátká paměť](/aktuality/kratka-pamet/)
