+++
title = "Ing. Marcela Matoušková, MBA."
description = "Ing. Marcela Matoušková, MBA Kandidátka za Nezávislé pro Plasko"
og_image = "/img/people/matouskova.jpg"
image = "matouskova.jpg"
type = "people"
+++
_"Zeleň je jeden z hlavních důvodů, proč v Plasích rádi žijeme. Je třeba se o ni nadále starat, aby tu byla i pro naše děti a vnoučata."_

V Plasích žiju už několik desítek let a nikdy bych neměnila. Dokážu si představit, že bych mohla žít jinde, ale nechci.

Mám ráda sport, koneckonců hraju v Plasích volejbal už od nepaměti a jsem ráda, že se Sokolu podařilo připravit a začít realizovat důstojnější zázemí pro všechny sportovce.  Uvítala bych ale lepší spolupráci radnice se Sokolem, ale nejen s ním, ale i s ostatními organizacemi.

Otvírám pravidelně webové stránky Plas, abych se dozvěděla, co je nového a pak je zase zklamaně zavírám – stránky jsou nepřehledné, moc se nedozvím a na oprávněné dotazy občanů je nahlíženo jako na osobní útok…a přitom otevřená komunikace radnice je základním předpokladem pro její práci.

Členka Výboru TJ Sokol Plasy
