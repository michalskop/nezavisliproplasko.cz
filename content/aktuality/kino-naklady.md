+++
title = "Kino - skutečné náklady"
description = "Šok! Šířili jsme nepravdu! Náklady na kino nejsou 63 milionů, ale 70 milionů!"
author = "Michal Škop"
date = "2018-09-07T11:00:00+02:00"
tags = ["aktuality", "Škop"]
categories = ["aktuality"]
og_image = "/img/aktuality/kino_svejk_cut_wide.png"
+++
Finanční výbor města Plas 3.9.2018 schválil toto doporučení:

_"Finanční výbor pověřuje předsedu [p. Prantnera] zveřejnit na sociálních sítích a www města Plasy stanovisko FV k nepravdám šířeným na sociálních sítích ohledně nákladů na rekonstrukci "Kina" a struktury celkového úvěru."_ (viz [zápis ze zasedání](http://www.plasy.cz/e_download.php?file=data/editor/464cs_13.pdf&original=17_Finan%C4%8Dn%C3%AD%20v%C3%BDbor_09.03.2018.pdf))

Je pravda, že my o kině píšeme a zveřejňujeme informace:

V článku [Plasy - nejzadluženější město ČR](https://skop.eu/post/nejzadluzenejsi-mesto-cr-2018/) jsem psal, že _rekonstrukce "kina" a vybudování restaurace v něm_ stojí _**62 mil. Kč**_

V přetištěném článku v [Novinách pro Plasko](/docs/noviny.pdf) se píše, že _odsouhlasené náklady (restaurace + multifunkční centrum) činí **63 milionů Kč**_.

Vzhledem k onomu doporučení Finančního výboru jsem ještě jednou prošel to, co je o nákladech na akci "kino" dostupné. A **opravdu jsme _"šířili nepravdu"_ !**

Není to 62 nebo 63 milionů, jak jsme mylně informovali, ale po aktualizaci je to zhruba **67 až 70 milionů**.

Nepočítali jsme totiž s úvodními náklady na pořízení pozemku se stavbou, demolici původní stavby na něm a na projektovou dokumentaci k projektu. Také jsme nevěděli, že ještě naroste cena za restauraci a že město bude chtít dát velkorysou půjčku na rozjezd "kina", o které lze velmi pochybovat, zda se někdy vrátí.

Celé jsme to shrnul v článku [Jak šel čas s cenou rekonstrukce kina](https://skop.eu/post/kino-2018/) - aneb jak se z původních 12 až 14 milionů v roce 2014 stalo během 4 let dnešních 67 až 70 milionů.
