+++
title = "Volte chytře"
description = "Volební systém pro komunální volby je složitý."
author = "Nezávislí pro Plasko"
date = "2018-09-19T11:00:00+02:00"
tags = ["aktuality", "noviny"]
categories = ["aktuality"]
og_image = "/img/aktuality/volebni_system.png"
+++
Systém komunálních voleb je velmi složitý. Můžete křížkovat celou kandidátku (“velký křížek”), ale také jednotlivé lidi z jednotlivých kandidátek (“malé křížky”).

V Plasích má každý 15 hlasů, to je síla “velkého křížku”. Také můžete dát 15 “malých křížků”, to však není moc chytrý postup. Oba postupy lze i kombinovat.

Hodně lidí si myslí, že zakřížkováním “malým křížkem” volí svého vybraného kandidáta. Ale tak to není. Zakřížkováním kandidáta volí nejprve stranu, za kterou daný člověk kandiduje. Křížky - hlasy se nejprve spočtou pro celou stranu, určí se, kolik mandátů strana získá, a teprve poté dojde na rozdělení v rámci strany.

### Příklad Pepy a Anežky
Pepa a Anežka žijí v městě Plasov, kde kandidují Bezva nezávislí a Strana slibů. Za Stranu slibů kandiduje na prvním místě pan Nepříjemný a na desátém místě paní Milá, známá Pepy a Anežky.
Pepa ví, že rozhodně nechce pana Nepříjemného. Ale paní Milou zná a tak jí dá ve volbách “malý křížek” a spokojeně hodí lístek do urny. Ve skutečnosti, aniž by to chtěl, dal jeden hlas panu Nepříjemnému. A 14 hlasů mu propadlo.

##### Jak to?
Zakřížkováním kandidáta se přeci volí nejprve strana, za kterou daný člověk kandiduje! A protože pan Nepříjemný byl vpředu a dostal celkem více hlasů než paní Milá, získal tím i Pepův hlas.

##### Anežka na to jde chytře.
Anežka má také ráda paní Milou, ale také se jí líbí, co plánují Bezva nezávislí. A hlavně ví, jak funguje volební systém. A než aby “omylem” dala hlas panu Nepříjemnému, raději dá “velký křížek” Bezva nezávislým. Tím jim právě dala 15 hlasů!

![Volební systém](/img/aktuality/volebni_system.png)
