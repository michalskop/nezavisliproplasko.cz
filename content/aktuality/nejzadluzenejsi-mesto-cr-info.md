+++
title = "Plasy - nejzadluženější město ČR"
author = "Michal Škop"
date = "2018-07-13T05:07:31+02:00"
tags = ["aktuality", "zastupitelstvo", "dluh"]
categories = ["aktuality"]
og_image = "/img/aktuality/kino_svejk_cut_wide.png"
+++
V České republice je 607 měst, z toho 150 hospodaří úplně bez dluhů, více jak polovina má jen malé dluhy a jen 35 měst mělo dluh vyšší jak 50 % svého ročního příjmu.

Ale **Plasy je všechny trumfují**. Letos už vedení města schválilo dluh více jak 109&nbsp;% našeho ročního příjmu, splácet se má **až do roku 2033**. Přečtěte si více na [speciální stránce o zadlužení města](https://skop.eu/post/nejzadluzenejsi-mesto-cr-2018/).

Dr. Michal Škop<br/>
[kandidát za Nezávislé pro Plasko](/nas-tym/)
