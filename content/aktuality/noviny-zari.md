+++
title = "Noviny pro Plasko - září 2018"
description = "Noviny pro Plasko ke stažení."
author = "Nezávislí pro Plasko"
date = "2018-09-18T11:00:00+02:00"
tags = ["aktuality", "noviny"]
categories = ["aktuality"]
og_image = "/img/aktuality/kino_svejk_cut.png"
+++
Celé Noviny pro Plasko vydání září 2018 ke stažení: [Noviny pro Plasko - září](/docs/noviny2.pdf)

Články:

* Plasy - město pro spokojený život
* Město pro všechny (Michal Škop)
* Sport a volný čas (Jan Jakeš)
* Dobrá škola je základ (Martin Fazekaš)
* Rozumné hospodaření (Václav Gross)
* Informace, otevřená radnice (Eva Pořádková)
* Volte chytře
* _Jak to všechno v Plasích začalo_
