+++
title = "Sport a volný čas"
description = "Sport a volný čas."
author = "Jan Jakeš"
date = "2018-09-21T11:00:00+02:00"
tags = ["aktuality", "noviny"]
categories = ["aktuality"]
og_image = "/img/aktuality/kids.jpg"
+++
To nejlepší pro život jsem se naučil při sportu, Jak se nevzdat, když to někdy nejde. Jak zatnout zuby, když to bolí. Jak si najít místo v týmu - kdy se porvat a kdy ustoupit.

Mám radost, že se Sokolu a Kopretině sport v Plasích daří rozvíjet. Že to tu neděláme vůbec špatně, dokazuje třeba ve fotbale Víťa Lavička a Bára Votíková. Když uděláme další oddíly, např. atletiku, vyroste nám tu možná jednou další Zátopek nebo Špotáková. Možná už tady ostatně mezi námi jsou. Jenom o nich nevíme, protože nikdy nedostali možnost svůj talent projevit.

Mým snem je, aby všichni lidé v Plasích, mladí i staří, sportovali a měli možnost si tady najít to, co je bude bavit a bude jim vyhovovat. O kroužcích to platí právě tak.

Investice do sportu a aktivit pro volný čas se městu vyplatí.

![Sport](/img/aktuality/kids.jpg)
