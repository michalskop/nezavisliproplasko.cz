+++
title = "Město pro všechny"
description = "Město pro všechny."
author = "Michal Škop"
date = "2018-09-24T11:00:00+02:00"
tags = ["aktuality", "noviny"]
categories = ["aktuality"]
og_image = "/img/aktuality/fotka1.jpg"
+++
Studoval jsem v Praze, v Německu i ve Španělsku, roky jsem cestoval po světě, ale nakonec jsem se stejně vrátil zpátky do Plas. Je tu prostě doma a je tu krásně. Ale jinde jsem okoukal, že by se tu dalo leccos zlepšit. Mým krédem je nevymýšlet znovu kolo, ale podívat se, kde ho umějí udělat nejlepší.

Je pro mě třeba důležitá dobrá škola, protože mám dvě malé děti. A tady mám velkou inspiraci ve Finsku, které se ve srovnáních opakovaně umísťuje mezi nejlepšími. Nechtěli byste, aby vaše děti patřily ke špičce v Evropě, ve světě?  

V práci se věnuji  “otevírání dat”, což v praxi znamená zveřejňování nejrůznějších dokumentů, smluv, atd., aby to k něčemu bylo. To je mimochodem zase trochu podle Estonska a Velké Británie. V ČR už tím šetříme doslova miliardy. Plasy podle zákona zveřejňovat nic moc nemusí. Ale městská kasa je společná kasa a myslím, že lidé mají vědět, za co se utrácí a kolik.

Líbí se mi také, jak dobře leckde funguje zapojení lidí do života města. Máte dobrý nápad? Sem s ním. Město má každý rok vyhrazený kousek rozpočtu, aby se ty nejlepší nápady mohly stát skutečností.

Dětské hřiště? Nová lávka? Místo pro teenagery? Setkávání seniorů? Proč ne. Lidé sami hlasováním rozhodnou, který projekt zrealizovat. Už roky to funguje v zahraničí, dokonce hned za hranicemi v Polsku. Ale poslední roky také v Brně nebo v Praze. Rád bych to zavedl i u nás v Plasích.

![Open](/img/aktuality/fotka1.jpg)
