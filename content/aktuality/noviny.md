+++
title = "Noviny pro Plasko"
description = "Noviny pro Plasko ke stažení."
author = "Nezávislí pro Plasko"
date = "2018-09-01T11:00:00+02:00"
tags = ["aktuality", "noviny"]
categories = ["aktuality"]
og_image = "/img/aktuality/kino_svejk_cut.png"
+++
Celé Noviny pro Plasko ke stažení: [Noviny pro Plasko](/docs/noviny.pdf)

Články:

* Plasy - nejzadluženější město ČR 2018 (Michal Škop)
* Akce "Kino"
* Přidružené obce Plas - financování (Václav Gross)
* Krátká paměť (Martin Fazekaš)
* Jak nás město (ne)informuje (Eva Pořádková)
* Sport v Plasích (Jan Jakeš)
* _Nový pavilon ZŠ_
