+++
title = "Vodovod v Žebnici"
description = "V. Gross: Vzhledem k množícím se dotazům ohledně realizace vodovodu v Žebnici po červnovém jednání Zastupitelstva města Plasy jsem se rozhodl pro napsání tohoto článku, ve kterém bych rád shrnul dosavadní průběh celé akce."
author = "Václav Gross"
date = "2018-07-18T21:11:31+02:00"
tags = ["aktuality", "zastupitelstvo", "žebnice"]
categories = ["aktuality"]
og_image = "/img/aktuality/vodovod.png"
+++
_„Voda je nejzákladnější podmínkou pro život lidí“._

Říká se, že příští války nebudou o ropu, nerostné suroviny, ale o vodu.

Vzhledem k množícím se dotazům ohledně realizace vodovodu v Žebnici po červnovém jednání Zastupitelstva města Plasy jsem se rozhodl pro napsání tohoto článku, ve kterém bych rád shrnul dosavadní průběh celé akce.

V Žebnici se s nedostatkem a kvalitou pitné vody potýkáme již několik desetiletí. Zásobování vodou jednotlivých nemovitostí je ze soukromých studní, kde zejména v posledních letech je nedostatek vody – o kvalitě vody nemluvě. Již v osmdesátých letech minulého století se započaly první přípravy k zajištění vody do Žebnice. Začaly se zpracovávat první studie zabývající se hledáním možného zdroje s dostatečnou kvalitou a kapacitou pro zásobování obyvatelstva pro obce Horní Hradiště a Žebnice. Zdroj se nacházel u „Zelených“ (u Mladotic).

Po revoluci se nechávalo zpracovat několik hydrogeologických průzkumů, pořídila se studna „Na pískách“, která v minulosti zásobovala vojenský prostor na Hadačce. Voda v této studni bohužel vykazovala příliš vysoký obsah manganu a železa a úprava na pitnou vodu by byla komplikovaná. V roce 2004 se provedl zkušební vrt, který nakonec neměl dostatečnou kapacitu pro zajištění zásobování celé obce. Zvažovala se také varianta napojení na vodovod v Plasích, původně pro obě obce (Horní Hradiště a Žebnice), zde ale bylo problematické provedení potrubního vedení přes skalnatý terén „V Pekle“. Další variantou bylo napojení pouze Žebnice přes „Hlubočici“, kde jsme opět narazili na vysoké náklady za potrubní vedení z Plas a nutnost realizovat posilovací přečerpávací stanici. Tehdy se jednalo o částku cca 8 mil. Kč pouze za přívod do Žebnice.

V roce 2010 nás napadla myšlenka napojení se na soukromý vrt společnosti Kralovická zemědělská a.s, který je umístěný nedaleko od obce, a to, že je využíván pro potravinářské účely, bylo předpokladem, že by ho bylo možné využít jako zdroj pitné vody také pro Žebnici.

Ihned jsme začali jednat s představiteli této společnosti a dohodli jsme se na podmínkách, za kterých by nebyl problém tuto variantu zrealizovat a do budoucna provozovat.  Podmínkami bylo, že město na své náklady zajistí čerpací zkoušku k prokázání vydatnosti tohoto zdroje, jak pro zásobování provozu živočišné výroby v zemědělském družstvu, tak spotřeby pro obyvatele obce, a následně veškeré náklady s tím spojené půjdou za městem.

V září 2010 byla provedena čerpací zkouška se zjištěním, že zdroj pitné vody ve vlastnictví Kralovické zemědělské a.s. má využitelnou vydatnost 78 m3 – přitom denní spotřeba družstva a obce (170 obyvatel) je plánovaná na cca 30 m3. Zdroj má tedy více jak dvou násobnou kapacitu.  Chemickým rozborem byl zjištěn nepatrně vyšší obsah dusičnanů (o 5,2 mg/ l) a byla zde drobná odchylka pH (o 0,2) – což však není problém, který by se nedal vyřešit úpravnou.

Konečně nám svitla malá naděje, že se snad již podaří vodovod v Žebnici v dohledné době zrealizovat.

Začala se zpracovávat projektová dokumentace. Nejdříve studie, poté dokumentace pro územní řízení, stavební povolení a nakonec dokumentace pro výběrové řízení.  Také mezi městem a všemi občany v Žebnici podepsala dohoda o spolupráci s tím, že město Plasy myslí realizaci vážně a na druhé straně od občanů bylo vyžadováno, aby si nechali zpracovat projektovou dokumentaci na přípojky, které budou navazovat na vodovodní řád a následně po realizaci zůstanou v majetku vlastníka připojené nemovitosti.

Jen pro informaci - cena za PD jedné přípojky k nemovitosti činila cca 2500 Kč a každý si ji hradil sám. Projekty byly zpracovány, stavební povolení vydáno, vše bylo zapracováno do PRVKPK (Plán rozvoje vodovodů a kanalizací v Plzeňském kraji). Podmínkou ze strany města bylo dostatečné množství finančních prostředků a získání dotace na tuto akci – původně 70%, které následně bylo revokováno pouze na získání dotačních prostředků na realizaci.

Rozpočtové náklady celé akce dle projektové dokumentace (vodojem, napojení, rozvodný řád, přípojky) činily 19,3 mil Kč (cena bez DPH). Vzhledem ke skutečnosti, že se pro město Plasy bude jednat o výdělečnou činnost (voda se bude prodávat), uvádím veškeré částky bez DPH, protože DPH nebude nákladovou položkou města Plasy za tuto akci.

Zároveň Osadní výbor v Žebnici začal ze svých přidělovaných prostředků ve výši 250 tis. Kč /rok šetřit peníze, které by bylo možné využit na tuto realizaci. Řada akcí byla pozastavena do doby, dokud nebude zajištěna pitná voda v dostatečném množství a kvalitě.

 Město Plasy vyhlásilo v roce 2016 veřejné výběrové řízení na dodavatele stavby, kterého se zúčastnilo 9 společností a nejnižší cenu nabídla společnost BP stavby s.r.o. za cenu 9,3 mil Kč (48 % rozpočtových nákladů dle projektové dokumentace).

V roce 2016 jsme se také začali intenzivně věnovat vyhledáváním dotačních titulů, které by mohly být využity pro náš případ, ať to bylo Ministerstvo životního prostředí prostřednictvím fondu SFŽP, Ministerstvo zemědělství nebo Plzeňský kraj. Podmínky dotace ze SFŽP nebyly zcela optimální pro naši realizaci a od podání žádosti se nakonec upustilo.

V roce 2017 se podala první žádost na Krajský úřad Plzeňského kraje, z důvodu podmínek pro realizaci (profinancovat v daném roce) pouze na první etapu ve výši cca 3 mil. Kč (vodojem, úpravna vody a hlavní přívod do vodojemu).  Plzeňský kraj naši žádost vyřadil z absurdních důvodů, že napojení domácností v Žebnici pokrývá pouze 7 % celkového počtu obyvatel v rámci celého města Plasy a dále, že jsme byli jako město příjemci dotace v předešlém roce.

V září 2017 jsme podali ještě jednu žádost na kompletní realizaci na Ministerstvo zemědělství, kde doposud nebylo rozhodnuto. Pouze víme, že nás nevyřadili a vyžádali si další doplňující materiály - opakovanou čerpací zkoušku a podrobný chemický rozbor vody s termínem do 31.08.2018.

Nyní čekáme na sklizeň řepky, která je v okolí vrtu, a po sekání proběhne opakovaná čerpací zkouška. Zároveň jsme v letošním roce opět podali žádost na Plzeňský kraj o příspěvek na realizaci (opět pouze 1. etapy – aby bylo možné tuto část zrealizovat včetně vyúčtování dle podmínek dotačního titulu v letošním roce do 30.11.2018). Plzeňský kraj po konzultaci s Ministerstvem zemědělství nám na svém červnovém jednání zastupitelstva přidělil částku 759 tis. Kč. Což bylo cca 50 % toho, co jsme očekávali dle podmínek PK a jak bylo schváleno pro jiné obdobné akce v regionu. Na dotaz, proč tak málo, nám sdělili, že očekávají, že zbytek (možno až do 80 % uznatelných nákladů) obdržíme od Ministerstva zemědělství. Samozřejmě tohle sdělení je bez záruky.

Na jednání červnového zastupitelstva města Plasy však padlo úplně jiné rozhodnutí, než jaké jsem očekával a bylo překvapením i pro občany Žebnice. ZM schválilo usnesení nepřijmout dotaci od Plzeňského kraje a vyčkat na rozhodnutí Ministerstva zemědělství, které proběhne možná do konce roku 2018.

Chápu, že závazek zahájit celou akci a nemít jistotu, zda obdržíme dotační finanční příspěvek v celé očekávané výši, je pro město riziko. Ale v době, kdy si půjčujeme na akce, které jsou svým charakterem také rizikové, zejména z hlediska budoucího využití a ekonomické rentability, se mi zdá toto krátkozraké.
Ještě by pro mě bylo přijatelné, kdybychom se věcně bavili o tom, že nám momentálně chybí finanční prostředky na realizaci.

Ale argumenty, které na jednání tohoto zastupitelstva také padly, že nejsou známy problémy občanů (kromě jedinců) v Žebnici s vodou, že nemáme jistotu, zda občané budou vodu odebírat, že bude voda drahá atd., se mi zdají absurdní. To, že se tímto problémem občané, volení zástupci OV, volení zástupci města Plasy zabývají bezmála 40 let, tomu nenasvědčuje. Jenom náklady utracené za veškeré přípravné práce (projekty, průzkumy, zkoušky, rozbory, posudky, atd.) již představují částku téměř 1 mil. Kč.

Závěrem věřím v to, že se pohled na stav zásobování pitnou vodu v obci Žebnice ze strany Města Plasy změní a že dlouho očekávaná realizace se v dohledné době podaří uskutečnit ke spokojenosti všech občanů v Žebnici.

Ing. Václav Gross<br/>
člen OV Žebnice a místostarosta obce Plasy<br/>
[kandidát za Nezávislé pro Plasko](/nas-tym/)<br/>
článek do Plaského zpravodaje
