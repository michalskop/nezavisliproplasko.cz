+++
title = "Jak nás město neinformuje"
description = "Takto si představuji budoucí fungování města - otevřenost, transparentnost, slušnost."
author = "Eva Pořádková"
date = "2018-09-01T11:11:00+02:00"
tags = ["aktuality", "Pořádková"]
categories = ["aktuality"]
og_image = "/img/aktuality/skola.jpg"
+++
V Plasích jsem se narodila a s výjimkou období vysokoškolského studia zde žiji celý svůj život. Budoucnost našeho města mi není lhostejná a proto mě znepokojuje, jakým směrem se v současnosti ubírá.

Oceňuji, že se zde v posledních letech udělalo mnoho věcí, které vedly ke zvelebení města. Avšak mnoho z nich se dělo z mého pohledu živelně, nekoncepčně, bez celkové vize. Zářmým příkladem je transformace kina v multikulturní zařízení. Mnohamilionový projekt, o jehož podobě, budoucím využití a celkové reálné ceně se s občany nevedla žádná otevřená diskuze.

Obecně vzato, dnešní informovanost o dění ve městě a na radnici je velmi kusá. Pár informací na webových stránkách, hlášení místního rozhlasu, zpravodaj pouze 4x ročně. Žijeme v 21. století, v době moderních technologií. V době, kdy pro mnoho obcí je distribuce důležitých informací mezi občany zcela samozřejmě zajištěna například formou SMS InfoKanálu či emailového InfoSystému. Starostové otevírají radnice, vypisují oficiální termíny pro setkávání se s občany, zajímají se o jejich názor. Takto si představuji budoucí fungování města - otevřenost, transparentnost, slušnost.
