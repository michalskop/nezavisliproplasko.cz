+++
title = "Upozaděné vzdělávání"
description = "V době, kdy sílí populační ročníky, kdy se do Plas stěhují a znovunavracejí mladí lidé zakládající rodiny, my upřednostníme kulturu před vzděláním."
author = "Eva Pořádková"
date = "2018-09-02T11:00:00+02:00"
tags = ["aktuality", "Pořádková"]
categories = ["aktuality"]
og_image = "/img/aktuality/skola.jpg"
+++
V Plasích jsem se narodila a s výjimkou období vysokoškolského studia zde žiji celý svůj život. Pohled malého dítěte, hrajícího si bezstarostně na pískovišti, však nyní vystřídal pohled dospělého, kriticky uvažujícího člověka, kterému není budoucnost našeho města lhostejná. Nacházím se v období jakéhosi bilancování a kladu si mnoho otázek týkajících se aktuálního dění ve městě a na radnici.

V Plasích se v posledních letech udělalo bezpochyby mnoho věcí, které vedly k jejich zvelebení. Avšak mnoho z nich se dělo z mého pohledu živelně, nekoncepčně, bez celkové vize, kam by mělo město směřovat. Jistou odpověď, kam se město ubírá, mi dává nyní probíhající rekonstrukce místního kina a jeho jakási transformace v multikulturní zařízení. Akce, která se z řádu několika jednotek milionů vyšplhala na mnoho desítek milionů, bez toho, aby se o tom vedla nějaká hromadná diskuze, bez průzkumu trhu, bez občanů.

„Rekonstruuje se kino, víš o tom?“ ptám se. „Ano, vím“, odpovídají mi lidé na potkání. „A víš také, kolik to všechno bude stát?“, ptám se dál. „Tak samozřejmě. Nebude to levné, město si vezme úvěr a za pár let je to splacené.“ Hm, pár let. Já to vidím jinak.

Nechci se tady rozepisovat o konkrétních částkách, od toho jsou tu jiní – ekonomové, analytici atd. Já to vidím tak, že na úkor vzdělání jsme upřednostnili kulturu. Protože jak jinak si vysvětlit, že město prodalo základní škole půdu doslova pod nohama, slíbilo nový moderní pavilon. A ejhle, žádná škola, žádné plány, vzdělání nechme stranou, však oni si poradí. Kultura má teď prioritu č. 1, ne vzdělání.

V době, kdy sílí populační ročníky, kdy se do Plas stěhují a znovunavracejí mladí lidé zakládající rodiny, my upřednostníme kulturu před vzděláním.

Plasy mají sice statut města, ale v očích ostatních jsme „pouzí“ venkované. Nedovolme, aby byl venkov spojován s nižší kvalitou vzdělání. Naopak, snažme se udělat maximum pro to, aby naše děti měly adekvátní, motivující prostředí pro výuku. Vytvořme jim takové podmínky a zázemí, aby uspěly v konkurenci s jinými, mnohdy většími, věhlasnějšími a jistě i štědřeji financovanými školami.
