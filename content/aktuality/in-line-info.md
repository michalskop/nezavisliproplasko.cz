+++
title = "Informace o plánované in-line dráze"
author = "Michal Škop"
date = "2018-06-26T13:07:31+02:00"
tags = ["aktuality", "zastupitelstvo"]
categories = ["aktuality"]
+++
Připravovaná in-line dráha na Velké louce a dál směrem ke studánce vzbuzuje v Plasích dost vášní. Dost lidí kritizuje to, že se má současná oblíbená vycházková cesta vyasfaltovat. Že se rozhoduje **o nás, bez nás**.

Abychom věděli, o čem se bavíme, vyžádali jsme si dostupné podklady a dali je na [speciální stránku o in-line dráze](https://skop.eu/post/in-line-2018/).

Dr. Michal Škop<br/>
[kandidát za Nezávislé pro Plasko](/nas-tym/)
