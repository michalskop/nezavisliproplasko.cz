+++
title = "Rozumné hospodaření"
description = "Rozumné hospodaření."
author = "Václav Gross"
date = "2018-09-22T11:00:00+02:00"
tags = ["aktuality", "noviny"]
categories = ["aktuality"]
og_image = "/img/aktuality/zebnice.jpg"
+++
Již osmým rokem působím v Plasích na pozici místostarosty a mojí doménou jsou přidružené obce. Mohu říci, že se v těchto okolních obcích podařilo uskutečnit řadu akcí, a to i přes značně omezený rozpočet, který mají k dispozici. Trvale se snažíme maximálně využít veškeré finanční prostředky. Naše babičky tomu říkaly "každou korunu v dlani dvakrát obrátit". Pořizujeme svépomocí, co lze, důsledně provádíme výběrová řízení a především využíváme co nejvíce dotace. Máme proto zpracovanou projektovou dokumentaci na řadu projektů, abychom byli připraveni, až bude vypsaný vhodný dotační titul.

Do nadcházejících voleb jdu tentokrát s novým týmem (s Nezávislými pro Plasko), ale můj směr zůstává stejný: pokračovat v prosazování zájmů okolních obcí, například rozumným navýšením jejich ročního rozpočtu. Rovněž bych se rád dál věnoval finančním záležitostem města. Zkušenosti s efektivním využíváním peněžních prostředků se budou v následujících letech v Plasích velmi hodit.

![Sport](/img/aktuality/zebnice.jpg)
