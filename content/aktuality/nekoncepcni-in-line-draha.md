+++
title = "Nekoncepční in-line dráha"
description = "Já, jakožto aktivní rekreační inlinistka, jsem o in-line dráze v Plasích snila již mnoho let. Leč současný návrh, který se mi dostal do rukou, zůstal kdesi na půli cesty."
author = "Eva Pořádková"
date = "2018-08-28T11:00:00+02:00"
tags = ["aktuality", "Pořádková"]
categories = ["aktuality"]
og_image = "/img/aktuality/inline_nyrsko.jpg"
+++
Když jsem psala o nekoncepčnosti, ráda bych se ještě zmínila o projektu in-line dráhy, který nyní vedení města projednává.

Já, jakožto aktivní rekreační inlinistka, jsem o in-line dráze v Plasích snila již mnoho let. Inspirována jinými areály po republice, počínaje velkými městy jako je Brno, až po menší města typu Konstantinovy Lázně. Tam se povedlo vybudovat krásnou in-line dráhu vedoucí po okraji města, lemující pole, jejímž základem je tříkilometrový okruh a několik slepých ramen. Tento projekt nazvaný Na kolečkách konstantinolázeňskem dokonce vyhrál v cestovatelské soutěži ROP Jihozápad.

A já se ptám, proč bychom se nemohli nechat inspirovat? Areál Velké louky je úžasnou příležitostí, k podobnému projektu vyloženě vybízí. Leč současný návrh, který se mi dostal do rukou, zůstal kdesi na půli cesty. Celá koncepce podle mého názoru absolutně nevyhovuje tomu, jak by měla in-line dráha vypadat, ať jde o umístění, šířku, spoluužívání s chodci, … Z hlediska bezpečnosti velmi rizikové, o komfortu při jízdě nemůže být řeč.

Mám za to, že areál Velké louky by se měl pojmout jako jeden velký celek, jako sportovně-rekreační areál, který bude sloužit k aktivnímu odpočinku široké veřejnosti. Je tolik možností, co by se zde dalo vybudovat a vytvořit, počínaje in-line dráhou v podobě uzavřeného okruhu s jednosměrným provozem odděleným od zóny pro pěší, kočárky, pejsky atd, až po prvky venkovní posilovny, dětských atrakcí.

K tomu samozřejmě zázemí v podobě občerstvení, případně i půjčovny. Takovou možnost sportovního vyžití by jistě uvítali kromě domorodců i návštěvníci Plas, kteří se sem sjíždějí z blízkého i dalekého okolí. Ale udělat kousek dráhy odněkud někam bez provázanosti s dalšími kroky, které by časem navazovaly, to raději veďme ještě diskuzi a zvažujme další alternativy a možnosti. Koupaliště v Plasích byla také jistě myšlenka dlážděná dobrými úmysly, ale…

Je toho mnohem více, o čem by se dalo psát, ale nechci na Vás v těchto prázdninových dnech přenášet negativní emoce. Naopak, ráda bych Vám popřála krásný zbytek léta a víru v to, že vždycky se dá něco změnit.

![in-line KL](/img/aktuality/inline_kl.jpg)
_in-line dráha u Konstantinových Lázní_
