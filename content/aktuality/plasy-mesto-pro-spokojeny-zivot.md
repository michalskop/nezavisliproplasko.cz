+++
title = "Plasy - město pro spokojený život"
description = "Plasy - město pro spokojený život."
author = "Nezávislí pro Plasko"
date = "2018-09-25T11:00:00+02:00"
tags = ["aktuality", "noviny"]
categories = ["aktuality"]
og_image = "/img/aktuality/fotka1.jpg"
+++
Jsme malé město a to se nám líbí. Žijeme tu s dětmi, blízko rodičů, často i prarodičů. Žijeme tu mezi přáteli a známými.

Nechceme být jako lidé v Praze, kde ani neví, jak se sousedé jmenují. K dětem si nenajímáme chůvy, hlídají nám je babičky a kamarádky. Když potřebujeme žebřík, míchačku nebo cokoliv jiného, zřídka je první myšlenka "pojedu to koupit". Hned přemýšlíme, kdo by to mohl mít, kdo nám to půjčí. Auto z příkopu nám vytáhnou chlapi s traktorem, protože odtahovky, to by se jeden nedoplatil.

Jsme tu v tom spolu. V nouzi táhneme za jeden provaz. Právě to dělá z Plas mnohem lepší místo pro život než jsou větší a bohatší města

Je podle nás potřeba se o dobré vztahy víc starat a podporovat je. Cítíme, že radnice s lidmi málo mluví. Málo ví, co lidi pálí. Že si taky často sama rozhodne a lidi postaví až před hotovou věc: takhle to prostě bude. Není divu, že pak vznikají problémy a hádky, protože se zjistí, že se zapomnělo na tohle a támhleto se mělo udělat jinak. Jeden je naštvaný na druhého. Sami víte, jak dlouho trvají sousedské spory, když se rozhoří.

Myslíme si, že tudy cesta k pohodovému životu nevede. Je potřeba to dělat jinak. Je potřeba se na společných věcech domluvit. Abychom tu i v budoucnu žili mezi přáteli a známými a měli si od koho půjčovat žebříky a míchačky.

![Open](/img/aktuality/fotka1.jpg)
