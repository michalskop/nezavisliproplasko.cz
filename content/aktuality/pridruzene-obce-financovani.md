+++
title = "Přidružené obce města Plasy – financování"
description = "Součástí města Plasy je také 5 přidružených obcí – Babina, Horní Hradiště, Lomnička, Nebřeziny a Žebnice."
author = "Václav Gross"
date = "2018-09-02T12:00:00+02:00"
tags = ["aktuality", "Gross"]
categories = ["aktuality"]
og_image = "/img/aktuality/vodovod.png"
+++
Součástí města Plasy je také 5 přidružených obcí – Babina, Horní Hradiště, Lomnička, Nebřeziny a Žebnice. V těchto obcích jsou ustanoveny Osadní výbory jako poradní orgány pro město Plasy. Pravidelně zasedají a dávají doporučující stanoviska za jednotlivé obce a vyřizují drobné záležitosti.

V posledních cca 10 letech jsou v ročním rozpočtu města Plasy pro každou obec plánovány, na zajištění investičních akcí a případně opravy, finanční prostředky ve výši 250 tis. Kč na rok. S tím, že ušetřené peníze je možné převést do dalšího období. Za posledních 8 let byly mimořádně uvolněny navíc prostředky na místní komunikace (3 x v Horním Hradišti, 1 x v Lomničce).

Tyto prostředky jsou navíc využívány také jako spoluúčast v případě realizace pří získání dotace. Osadní výbory se snaží s těmito prostředky hospodařit k maximálnímu využití, jak důkladnou připraveností akcí, důsledným zajišťováním výběrových řízení s důrazem na cenu a také tím, že řadu akcí jsou schopni zajistit svépomocí, pouze za nákup materiálu. Za tu dobu se podařilo v našich přidružených obcích realizovat nemalou řadu akcí, jako jsou sportovní hřiště, chodníky, opravy obecních budov, bezdrátový rozhlas, podpora výstavby DČOV a mnoho drobných akcí, také jsou zpracované, případně se zpracovávají projektové dokumentace pro připravované akce atd.
Je třeba však připomenout, že za posledních 10 let částka 250 tis. Kč zůstala stejná.

Přestože v těchto obcích žije cca 30 % občanů města Plasy, prostředky uvolňované do těchto částí z rozpočtu města v posledních letech tomu zdaleka neodpovídají. Přitom potřeby na obcích jsou obdobné jako v Plasích – komunikace, chodníky, vodovody, likvidace odpadních vod (ČOV, DČOV) atd. Dlouhodobě na tuto situaci upozorňuji, avšak ani prosincový návrh na jednání zastupitelstva zvýšit příspěvek pro obce alespoň z 250 tis. Kč na 300 tis. Kč nebyl schválen s komentářem, že na obcích je peněz dost a případné mimořádné akce profinancujeme individuálně. Mám obavy, že některé akce se oddálí a některé se nám nepodaří zahájit vůbec.

Velké akce, které nás v nejbližším období čekají, jsou vodovod v Žebnici a kanalizace v Nebřezinech. Také bychom se měli zabývat bezpečným průjezdem v našich obcích – komunikace, kde nás čekají minimálně chodníky - přes obec Horní Hradiště, Nebřeziny a po realizaci vodovodu také v Žebnici u kostela. V Babině je nutné opravit střechu nad obecním domem (bývalá škola) a v Lomničce se z velkých akcí připravuje revitalizace vodní nádrže v obci. Dále se připravují akce pro zadržování vody v krajině, ať je to v Nebřezinech nebo v Žebnici, kde lze očekávat v blízké době vypsání dotačních titulů a bylo by vhodné této příležitosti využít.

Jsem si vědom skutečnosti, že nás nečeká lehké období v oblasti financování vzhledem k zadluženosti města. Také nás čekají v Plasích velké akce – lékařský dům, pavilon školy atd., kde bude potřeba dalších prostředků. Zároveň nejsem ochoten přijmout skutečnost, aby přidružené obce doplácely na chybná rozhodnutí města v minulosti, a budu nadále usilovat o to, aby se situace ze strany města v oblasti financování k obcím změnila.
