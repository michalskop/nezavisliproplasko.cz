+++
title = "Sport v Plasích"
description = "Je s podivem, že právě v dnešní době, kdy je pohyb u dětí potřeba podporovat víc než kdy předtím, je sport v Plasích dlouhodobě opomíjen a podfinancován."
author = "Jan Jakeš"
date = "2018-09-03T11:00:00+02:00"
tags = ["aktuality", "Jakeš"]
categories = ["aktuality"]
og_image = "/img/aktuality/fotbal.jpg"
+++
Sport v Plasích má dlouholetou tradici. U dětí a mládeže má své nezaměnitelné místo. Nejde jen o fyzickou kondici. Sportováním děti získávají také psychickou odolnost - učí se prosadit v konkurenci, smířit se s prohrou, umět přijmout kritiku. Zároveň poznávají cenu dlouhodobé usilovné práce, sílu kamarádství a spolupráce, radost ze společných vítězství. Každý psycholog vám potvrdí, jak klíčové jsou pro mladého člověka takové hodnoty. Bez nich nám vyrůstá generace znuděných mladých povalečů, vandalů a egoistů, jak to vidíme ve větších městech.

Je s podivem, že právě v dnešní době, kdy je pohyb u dětí potřeba podporovat víc než kdy předtím, je sport v Plasích dlouhodobě opomíjen a podfinancován. Štědře se investuje do socialistických kulturáků, ale když TJ Sokol požádá o dotaci na topení, střechu či nedejbože novou budovu, tak město nemá peníze. Takovou politiku považuji za krátkozrakou a dlouhodobě neudržitelnou.
