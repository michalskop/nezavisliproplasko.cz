+++
title = "Finanční situace Plas z pohledu místostarosty"
author = "Václav Gross"
date = "2018-07-23T06:42:31+02:00"
tags = ["aktuality", "zastupitelstvo", "dluh"]
categories = ["aktuality"]
+++
Proč jsem hlasoval proti přijetí úvěru a jaká je finanční situace pro následující období v Plasích dle mého laického ekonomického názoru.

Zastupitelstvo města na svých jednáních v posledním období v březnu 2018 schválilo čerpání úvěru ve výši skoro **62 mil. Kč** a v červnu 2018 pověřilo starostu města k jednáním o dalším úvěru do výše **10 mil Kč**. V případě schválení čerpání dalšího úvěru se dostáváme do situace, kdy zadluženost obce bude k 31.12.2018 činit cca 110 % celkových průměrných příjmů za poslední čtyři roky.

Na co se nové úvěry budou čerpat – k tomu se v tomto článku nevyjadřuji.
Splátkový kalendář je stanoven na 15 let – což při 4 letém volebním období představuje zátěž pro **následující 4 volební období**.  Měsíční splátka ve výši 344 tis. Kč a v případě přijetí dalšího úvěru (+ 60 tis. Kč) – celkem tedy cca 404 tis. Kč měsíčně + úroky (pro rok 2019 - ve výši cca 70 tis. Kč měsíčně).

V roce 2017 byl Parlamentem ČR schválen zákon č. **[23 /2017 Sb. – Zákon o pravidlech rozpočtové odpovědnosti](https://www.zakonyprolidi.cz/cs/2017-23)** k zajištění financí v dlouhodobě udržitelném stavu u veřejných institucí mj. obcí.

Cílem tohoto zákona je udržet finanční prostředky ve zdravém stavu, kdy je možné dluh splácet a nevystavovat obec do dluhové pasti.

Zde [citace zákona - § 17](https://www.zakonyprolidi.cz/cs/2017-23#p17):
<div class="alert alert-info">
<p>(1) Územní samosprávný celek hospodaří v zájmu zdravých a udržitelných veřejných financí tak, aby výše jeho dluhu nepřekročila k rozvahovému dni 60 % průměru jeho příjmů za poslední 4 rozpočtové roky.

(2) Překročí-li dluh územního samosprávného celku k rozvahovému dni 60 % průměru jeho příjmů za poslední 4 rozpočtové roky, územní samosprávný celek je povinen jej v následujícím kalendářním roce snížit nejméně o 5 % z rozdílu mezi výší svého dluhu a 60 % průměru svých příjmů za poslední 4 rozpočtové roky.

(3) Nesníží-li územní samosprávný celek svůj dluh a jeho dluh k následujícímu rozvahovému dni převyšuje 60 % průměru jeho příjmů za poslední 4 rozpočtové roky, ministerstvo v následujícím kalendářním roce rozhodne podle zákona o rozpočtovém určení daní o pozastavení převodu jeho podílu na výnosu daní.
</div>

Z uvedeného vyplývá, že město Plasy se tímto rozhodnutím dostalo do **rizikové skupiny obcí** a budeme po celou dobu, než se dostaneme na částku minimálně 60% zadlužení z příjmové části rozpočtu, pod detailnějším dohledem z Ministerstva financí a **tento stav bude trvat cca 8 let**.

Do té doby bude nutno úvěr splácet. Je zde samozřejmě možné si vyjednat další půjčku, ale tím by se situace dále zhoršila.

V době, kdy nás čeká řada dalších ne zcela investičně levných akcí (Lékařský dům, Přístavba školy, Areál Velká louka, další rozšíření infrastruktury v Plasích, Vodovod v Žebnici, Kanalizace a vodovod v Nebřezinech, atd.), kde budou nutné další finanční prostředky minimálně ve formě **vlastního podílu k získaným dotačním prostředkům**, se dostáváme do ne zcela jednoduché situace. V rozpočtovém výhledu se sice počítá s finančními prostředky na investice a opravy, ale v případě realizace větší akce budou možnosti značně omezené.

Po volbách přijde nové složení ZM a vedení obce a tímto naším přístupem je dostáváme do nelehké situace.

Ing. Václav Gross<br/>
Zastupitel a místostarosta obce<br/>
[kandidát za Nezávislé pro Plasko](/nas-tym/)<br/>
článek do Plaského zpravodaje
