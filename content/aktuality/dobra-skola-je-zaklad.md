+++
title = "Dobrá škola je základ"
description = "Dobrá škola je základ obce."
author = "Martin Fazekaš"
date = "2018-09-20T11:00:00+02:00"
tags = ["aktuality", "noviny"]
categories = ["aktuality"]
og_image = "/img/aktuality/skola.jpg"
+++
Říkám o sobě obvykle, že jsem obyčejný venkovský učitel. Baví mě pracovat se zvědavými dětmi. Mám radost, když se mi je podaří  něčím zaujmout a pro něco je nadchnout. Teď to třeba zrovna zkouším s 3D tiskem. Takové nadšení si pak člověk často nese po celý život. Už u malého školáčka vidíte, že z něj jednou “něco bude”. V dnešní době jsou pro šikovné lidi možnosti, o kterých se naší generaci ani nesnilo.

Rád bych, aby se do nové radnice dostali lidé, kteří si váží vzdělání a budou ho náležitě podporovat. Skvělá škola v Plasích není nic nemožného. S podporou města to půjde. Dokážete si představit, že za deset či patnáct let budou Plzeňáci vozit děti do školy k nám?

![Škola](/img/aktuality/skola.jpg)
