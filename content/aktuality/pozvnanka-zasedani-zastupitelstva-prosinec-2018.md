+++
title = "Pozvánka na zasedání Zastupitelstva města Plasy 12.12.2018"
description = "Dobrá škola je základ obce."
author = "Nezávislí pro Plasko"
date = "2018-12-09T11:00:00+02:00"
tags = ["aktuality", "noviny"]
categories = ["aktuality"]
og_image = "/img/aktuality/skola.jpg"
+++

## Rozpočet na rok 2018
plánované výdaje 120 milionů a na rozšíření školy a školky zase ani koruna. Hlavně, že máme školství a vzdělání jako prioritu.

## "In-line dráha"
Místo cestičky ke studánce se chystá asfaltová "in-line dráha" (co ale nebude moc in-line dřáhou), pokud ji nechceme, je pravděpodobně poslední šance to zastavit.

Informace o plánované "in-line dráze": https://skop.eu/post/in-line-2018/

## Zveřejňování smluv města
Nezávislí pro Plasko navrhují automatické zveřejňování všech smluv. Proč to radniční koalice nechce?

Celý návrh je zde: https://docs.google.com/document/d/1cshr0hyrVKRYsBzkMQD_MsoOx8oc5tPMx1_5f7XBKf0/edit
