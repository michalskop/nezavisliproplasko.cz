+++
title = "Informace, otevřená radnice"
description = "Informace, otevřená radnice."
author = "Eva Pořádková"
date = "2018-09-23T11:00:00+02:00"
tags = ["aktuality", "noviny"]
categories = ["aktuality"]
og_image = "/img/aktuality/open.jpg"
+++
Milí Plasáci, také vás občas nemile překvapí, když vám při vaření z ničeho nic vypnou elektrický proud nebo vodu? Nebo když vám někdo řekne, že byl o víkendu na bezva akci na Louce, ale vy jste o ní nevěděli? Mně se to bohužel stává docela často. A tomu bych do budoucna ráda předešla zřízením funkčního Infokanálu města.

Veškeré informace o plánovaných odstávkách vody či přerušení dodávek elektrické energie, uzavírkách ulic, termínech změny svozu odpadu, termínech zasedání zastupitelstva města, kulturních a společenských akcích a mnohé další užitečné informace spojené se životem ve městě by vám chodily přímo na váš mobilní telefon či do emailové schránky.

Věřím, že to bude přínosem nejen pro ty z nás, kteří dojíždíme za prací mimo Plasy, nemáme možnost denně obcházet místní vývěsky nebo posedávat v hospodě, přesto však chceme vědět, co se děje.

![Open](/img/aktuality/open.jpg)
