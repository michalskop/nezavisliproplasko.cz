+++
title = "Krátká paměť"
description = "Byl jsem však na posledním zastupitelstvu (červen 2018) velice udiven a také rozhořčen, když jsem z příspěvků některých kolegů zastupitelů, a hlavně vedení města zjistil, že zřejmě trpí kolektivní ztrátou paměti, co se daných slibů týče."
author = "Martin Fazekaš"
date = "2018-09-03T11:00:00+02:00"
tags = ["aktuality", "Fazekaš"]
categories = ["aktuality"]
og_image = "/img/aktuality/skola.jpg"
+++
(rozšířený text z [Novin pro Plasko](/aktuality/noviny))

Když se vedení města v roce 2015 chystalo prodat jeden pavilon Základní školy v Plasích gymnáziu, vlastně Plzeňskému kraji, bylo kolem toho spousta diskuzí a hlavně emocí. Jak nad samotným smyslem prodeje, tak i okolo stylu „neinformování“ dotyčných, tj. hlavně učitelského sboru a rodičů.

V průběhu různých veřejných i neveřejných jednání na podporu prodeje padla spousta slibů jednak od členů zastupitelstva, také od vedení města i od samotného pana starosty.

Hlasování o prodeji (přes jasný střet zájmů paní ředitelky Lorenzové) proběhlo, budova je prodána a peníze mělo město na svém účtu. Píši MĚLO, protože (přes můj návrh na zastupitelstvu uložit tyto peníze stranou a použít je pouze na financování stavby nového pavilonu školy) je vedení města začlenilo do rozpočtu a použilo ke snížení dluhu v tom roce.

Byl jsem však na posledním zastupitelstvu (červen 2018) velice udiven a také rozhořčen, když jsem z příspěvků některých kolegů zastupitelů, a hlavně vedení města zjistil, že zřejmě trpí kolektivní ztrátou paměti, co se daných slibů týče.

Proto bych jim touto cestou rád připomněl, že základní škola je v PROVIZORIU, který se dá s vypětím všech jejich sil vydržet tak dva, tři roky. V září 2018 se podle jednoho ze slibů měli po dvou letech čekání žáci a učitelé zabydlovat v novém pavilonu, bohužel nějaká městem najatá firma udělala chyby při podávání projektu! Tj. základní škola začíná třetí rok čekání na lepší budoucnost. ANO, naše děti ten nový pavilon (a hlavně větší prostory) opravdu nutně potřebují. To, že již nejsou prosby a nářky nad současným stavem ze školy slyšet, neznamená, že již nic nepotřebují, to se jenom věnují všemi silami smysluplnějším věcem (dětem) než boji á la Don Quijote s vedením města.

Na zastupitelstvu také zaznělo, že navrhované prostory byly přemrštěné, že to bude stát zbytečně moc peněz, a že by stačil jen malý pavilónek za pár miliónů!?! Jaký to kontrast s akcí kino, kde původní plány byly kolem 16-20 milionů (a v současnosti jsou odsouhlaseny ve výši cca 63 milionů). U školy je vždy lepší budovat s rezervou, aby se pak nemuselo složitě hledat, jak dostat více dětí do jedné malé třídy. V současné době máme problém i s přeplněností (rada města schvalovala kapacitní výjimku) mateřské školky, která se v horizontu pár let bude muset na náklady města rozšířit. A to vše při současné velké zadluženosti města, kdy volně použitelné finance odhaduji na cca maximálně 4 miliony ročně na všechny investiční akce města.

Každopádně je třeba před zadáním jakéhokoliv projektu ohledně naší základní školy partnersky projednat s vedením školy jejich aktuální potřeby a střednědobý výhled do budoucna.
