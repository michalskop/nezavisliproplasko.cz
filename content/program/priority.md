+++
title = "Priority"
type = "program"
icon = "fa fa-tint"
+++
Leccos vidíme jinak než dnešní vedení města. Jsme přesvědčeni, že třeba **vodovod v Žebnici** nebo **kanalizace v Nebřezinech** jsou daleko důležitější než třeba za stejné peníze hospoda provozovaná městem v budově kina.

Přečtěte si více:

* [Město pro všechny](/aktuality/mesto-pro-vsechny/) ([Michal Škop](/nas-tym/michal-skop/))

* [Vodovod v Žebnici](/aktuality/vodovod-zebnice/) ([Václav Gross](/nas-tym/vaclav-gross/))

* [Dobrá škola je základ](/aktuality/dobra-skole-je-zaklad/) ([Martin Fazekaš](/nas-tym/martin-fazekas/))

* [Krátká paměť](/aktuality/kratka-pamet/) ([Martin Fazekaš](/nas-tym/martin-fazekas/))

* [Sport a volný čas](/aktuality/sport-a-volny-cas) ([Jan Jakeš](/nas-tym/jan-jakes/))

* [Kino - skutečné náklady](/aktuality/kino-naklady/) ([Michal Škop](/nas-tym/michal-skop/))
