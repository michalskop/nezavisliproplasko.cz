+++
title = "Vzdělávání"
type = "program"
icon = "fa fa-child"
+++
**Školy, školka, kroužky, sport a kultura** musí být na prvním místě. Stojíme na prahu internetové a robotické revoluce, většina lidské práce se brzy změní. **Kvalitní vzdělání v širokém slova smyslu** bude to, co z velké části ovlivní schopnost se úspěšně zapojit do takového světa.

Přečtěte si více:

* [Město pro všechny](/aktuality/mesto-pro-vsechny/) ([Michal Škop](/nas-tym/michal-skop/))

* [Upozaděné vzdělávání](/aktuality/upozadene-vzdelani/) ([Eva Pořádková](/nas-tym/eva-poradkova/))

* [Dobrá škola je základ](/aktuality/dobra-skole-je-zaklad/) ([Martin Fazekaš](/nas-tym/martin-fazekas/))

* [Krátká paměť](/aktuality/kratka-pamet/) ([Martin Fazekaš](/nas-tym/martin-fazekas/))

* [Sport a volný čas](/aktuality/sport-a-volny-cas) ([Jan Jakeš](/nas-tym/jan-jakes/))
