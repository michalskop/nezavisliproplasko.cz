+++
title = "Zeleň a prostředí"
type = "program"
icon = "fa fa-tree"
+++
Zeleň ve městě i v okolí je jeden z hlavních důvodů, proč lidi v Plasích a okolí **rádi žijí**. Je třeba se o ni nadále starat. Pokud je třeba některé stromy ve městě kácet, tak to vždy musí mít dobrý důvod. A vysázet za ně nové. A taky je třeba něco udělat s koupalištěm.

Přečtěte si více:

* [Město pro všechny](/aktuality/mesto-pro-vsechny/) ([Michal Škop](/nas-tym/michal-skop/))

* [Méně odpadu = méně platíte](https://www.facebook.com/NezavisliProPlasko/photos/a.1775861405830644/1876182162465234/?type=3&theater) (Nápady pro Plasko)

* [Koupaliště](https://www.facebook.com/NezavisliProPlasko/photos/a.1775861405830644/1863988403684610/?type=3&theater)

* [Nekoncepční in-line dráha](/aktuality/nekoncepcni-in-line-draha/) ([Eva Pořádková](/nas-tym/eva-poradkova/))
