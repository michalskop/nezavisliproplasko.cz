+++
title = "Zdravotnictví"
type = "program"
icon = "fas fa-hospital-alt"
+++
Podporujeme rozšíření zdravotních služeb v Plasích. Bude nutno proto hledat externí zdroje, když na to rozpočet Plas nebude stačit.

Přečtěte si více:

* [Město pro všechny](/aktuality/mesto-pro-vsechny/) ([Michal Škop](/nas-tym/michal-skop/))
