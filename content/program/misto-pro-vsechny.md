+++
title = "Místo pro všechny"
type = "program"
icon = "fa fa-users"
+++
Je potřeba myslet na všechny. Něco jiného chtějí senioři, něco jiného rodiče s menšími dětmi. Pokud třeba v Plzni nebo Dobřanech funguje “senior-taxi”, proč to neokoukat a nepřenést sem? Venkovní fitness (stroje na cvičení) je dneska oblíben doslova v každém věku a přitom to zas není tak drahé.

Přečtěte si více:

* [Město pro všechny](/aktuality/mesto-pro-vsechny/) ([Michal Škop](/nas-tym/michal-skop/))

* [Rozumné hospodaření](/aktuality/rozumne-hospodareni/) ([Václav Gross](/nas-tym/vaclav-gross/))
