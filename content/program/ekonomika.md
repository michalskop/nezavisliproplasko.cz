+++
title = "Ekonomika"
type = "program"
icon = "fa fa-credit-card"
+++
Plasy jsou bohužel zadlužené. Zatímco současné vedení začínalo v roce 2010 jen s malým dluhem, dnes už je dluh přes 70 milionů (více než roční rozpočet města) a je **nejzadluženějším městem ČR**. Splácet se bude **do roku 2033**. Řekněme stop dalšímu zbytečnému zadlužování, buďme zodpovědnější.

Přečtěte si více:

* [Rozumné hospodaření](/aktuality/rozumne-hospodareni/) ([Václav Gross](/nas-tym/vaclav-gross/))

* [Plasy - nejzadluženější město ČR 2018](/aktuality/nejzadluzenejsi-mesto-cr-info/) ([Michal Škop](/nas-tym/michal-skop/))

* [Přidružené obce - financování](/aktuality/pridruzene-obce-financovani/) ([Václav Gross](/nas-tym/vaclav-gross/))

* [Finanční situace Plas z pohledu místostarosty](/aktuality/financni-situace-plas-gross/) ([Václav Gross](/nas-tym/vaclav-gross/))

* [Kino - skutečné náklady](/aktuality/kino-naklady/) ([Michal Škop](/nas-tym/michal-skop/))

* [Jak šel čas s cenou rekonstrukce kina](https://skop.eu/post/kino-2018/) ([Michal Škop](/nas-tym/michal-skop/))
