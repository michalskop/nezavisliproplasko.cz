+++
title = "Otevřené město"
type = "program"
icon = "fa fa-lock-open"
+++
**Všechny informace, podklady, dokumenty na internetu**, častější zpravodaj (viz Manětín), důležité zprávy do mobilu nebo emailem není nic, co by mělo být dneska překvapivé (viz Kaznějov). Nebo otevřená diskuse na stránkách města, rychlá a relevantní komunikace s občany. Jen to zatím nemáme.

Přečtěte si více:

* [Informace, otevřená radnice](/aktuality/informace-otevrena-radnice/) ([Eva Pořádková](/nas-tym/eva-poradkova/))

* [Všechno info na webu](https://www.facebook.com/NezavisliProPlasko/photos/a.1775861405830644/1884984664918317/?type=3&theater) (Nápady pro Plasko)

* [Lepší zpravodaj, lepší informovanost](https://www.facebook.com/NezavisliProPlasko/photos/a.1775861405830644/1900552260028224/?type=3&theater) (Nápady pro Plasko)

* [Jak nás město neinformuje](/aktuality/jak-nas-mesto-neinformuje/) ([Eva Pořádková](/nas-tym/eva-poradkova/))
