+++
title = "Váš hlas bude mít váhu"
type = "program"
icon = "fa fa-user-plus"
+++
**Nerušit, vládneme**. To je dnešní představa vedení města. Přitom plno lidí by rádo městu pomohlo nápady, zkušenostmi. Pravidelné ověřené ankety, návrhy přes internet přímo od lidí a na to navázaný participativní rozpočet (část rozpočtu, o které hlasují přímo občané) už jinde fungují velmi dobře. A budou i u nás.

Přečtěte si více:

* [Část rozpočtu pro nápady lidí (participativní rozpočet)](https://www.facebook.com/NezavisliProPlasko/photos/a.1775861405830644/1908527882563995/?type=3&theater) (Nápady pro Plasko)

* [Město pro všechny](/aktuality/mesto-pro-vsechny/) ([Michal Škop](/nas-tym/michal-skop/))

* [Informace, otevřená radnice](/aktuality/informace-otevrena-radnice/) ([Eva Pořádková](/nas-tym/eva-poradkova/))
